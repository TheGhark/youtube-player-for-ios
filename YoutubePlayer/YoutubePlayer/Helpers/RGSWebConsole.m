//
//  RGSWebConsole.m
//  YoutubePlayer
//
//  Created by Camilo Rodriguez Gaviria on 10/02/14.
//  Copyright (c) 2014 Red Gears Studio. All rights reserved.
//

#import "RGSWebConsole.h"

@implementation RGSWebConsole

+ (void) enable {
    [NSURLProtocol registerClass:[RGSWebConsole class]];
}

+ (BOOL) canInitWithRequest:(NSURLRequest *)request {
    if ([[[request URL] host] isEqualToString:@"debugger"]){
        NSLog(@"%@", [[[request URL] path] substringFromIndex: 1]);
    }
    
    return FALSE;
}

@end
