//
//  RGSWebConsole.h
//  YoutubePlayer
//
//  Created by Camilo Rodriguez Gaviria on 10/02/14.
//  Copyright (c) 2014 Red Gears Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RGSWebConsole : NSURLProtocol

+ (void)enable;

@end
