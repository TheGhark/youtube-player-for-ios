//
//  RGSYoutubePlayerViewController.m
//  YoutubePlayer
//
//  Created by Camilo Rodriguez Gaviria on 10/02/14.
//  Copyright (c) 2014 Red Gears Studio. All rights reserved.
//

#import "RGSYoutubePlayerViewController.h"
#import "RGSWebConsole.h"

CGFloat const kPollingInterval = 0.2f;
CGFloat const kSecondsToSeekWhilePlaying = 2.0f;
CGFloat const kSecondsToSeekWhilePaused = 0.5f;

enum YoutubeVideoPlaybackStatus {
    YoutubeVideoPlaybackStatusUnstarted = -1,
    YoutubeVideoPlaybackStatusEnded = 0,
    YoutubeVideoPlaybackStatusPlaying = 1,
    YoutubeVideoPlaybackStatusPaused = 2,
    YoutubeVideoPlaybackStatusBuffering = 3,
    YoutubeVideoPlaybackStatusVideoCued = 5
};

typedef NSInteger YoutubeVideoPlaybackStatus;

@interface RGSYoutubePlayerViewController () <UIWebViewDelegate>

@property (strong, nonatomic) IBOutlet UIWebView *webView;
@property (strong, nonatomic) IBOutlet UIButton *seekBackwardsButton;
@property (strong, nonatomic) IBOutlet UIButton *playButton;
@property (strong, nonatomic) IBOutlet UIButton *seekForwardsButton;
@property (assign, nonatomic) BOOL playing;
@property (strong, nonatomic) IBOutlet UILabel *durationLabel;
@property (strong, nonatomic) IBOutlet UILabel *currentPlaybackLabel;
@property (strong, atomic) NSTimer *pollingTimer;
@property (assign, nonatomic) BOOL durationAvailable;

+ (NSString *)formattedTimeForSeconds:(float)seconds;
- (IBAction)seekBackwards:(UIButton *)sender;
- (IBAction)play:(UIButton *)sender;
- (IBAction)seekForwards:(UIButton *)sender;
- (void)updatePlayback;
- (void)seek:(BOOL)seeksBackwards;

@end

@implementation RGSYoutubePlayerViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [RGSWebConsole enable];

    NSString *embedHTML = @"<html>\
	<body style='margin:0px;padding:0px;'>\
    <script type='text/javascript' src='http://www.youtube.com/iframe_api'></script>\
    <script type='text/javascript'>\
    function onYouTubeIframeAPIReady() {\
    ytplayer=new YT.Player('playerId',{\
    events:{ }\
    })\
    }\
    </script>\
    <iframe id='playerId' type='text/html' width='%0.0f' height='%0.0f' src='http://www.youtube.com/embed/%@?controls=0&enablejsapi=1&rel=0&playsinline=1&autoplay=1' frameborder='0'>\
	</body>\
    </html>";
    NSString *html = [NSString stringWithFormat:embedHTML, CGRectGetWidth(self.webView.frame), CGRectGetHeight(self.webView.frame), @"YDXUJAqWUS4"];
    [self.webView loadHTMLString:html baseURL:[[NSBundle mainBundle] resourceURL]];
}

#pragma mark - UIWebViewDelegate

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    NSLog(@"Failed: %@", error);
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"Started Loading");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"Finished Loading");
    
    
    if (self.pollingTimer) {
        [self.pollingTimer invalidate];
        self.pollingTimer = nil;
    }
    
    self.pollingTimer = [NSTimer scheduledTimerWithTimeInterval:kPollingInterval target:self selector:@selector(updatePlayback) userInfo:nil repeats:YES];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSLog(@"Loading request: %@", request);
    return YES;
}

#pragma mark - Private

+ (NSString *)formattedTimeForSeconds:(float)seconds {
    float hours = seconds/3600.0;
    float minutes = (ABS(hours - floor(hours)))*60;
    float secondsF = (ABS(minutes - floor(minutes)))*60.0;
    NSString *secondsString = (secondsF < 10) ? [NSString stringWithFormat:@"0%d", (int)secondsF] : [NSString stringWithFormat:@"%d", (int)secondsF];
    NSString *minutesString = (minutes < 10) ? [NSString stringWithFormat:@"0%d", (int)minutes] : [NSString stringWithFormat:@"%d", (int)minutes];
    NSString *hoursString = (hours < 10) ? [NSString stringWithFormat:@"0%d", (int)hours] : [NSString stringWithFormat:@"%d", (int)hours];
    
    return [NSString stringWithFormat:@"%@:%@:%@", hoursString, minutesString, secondsString];
}

- (IBAction)seekBackwards:(UIButton *)sender {
    [self seek:YES];
}

- (IBAction)play:(UIButton *)sender {
    if (self.playing) {
        [self.webView stringByEvaluatingJavaScriptFromString:@"ytplayer.pauseVideo()"];
    } else {
        [self.webView stringByEvaluatingJavaScriptFromString:@"ytplayer.playVideo()"];
    }
}

- (IBAction)seekForwards:(UIButton *)sender {
    [self seek:NO];
}

- (void)updatePlayback {
    NSString *playback = [self.webView stringByEvaluatingJavaScriptFromString:@"ytplayer.getCurrentTime()"];
    CGFloat playbackFloat = [playback floatValue];
    self.currentPlaybackLabel.text = [RGSYoutubePlayerViewController formattedTimeForSeconds:playbackFloat];
    
    if (playbackFloat > 0) {
        self.durationAvailable = YES;
        NSString *duration = [self.webView stringByEvaluatingJavaScriptFromString:@"ytplayer.getDuration()"];
        self.durationLabel.text = [RGSYoutubePlayerViewController formattedTimeForSeconds:[duration floatValue]];
    }
    
    NSInteger state = [[self.webView stringByEvaluatingJavaScriptFromString:@"ytplayer.getPlayerState()"] integerValue];
    
    switch (state) {
        case YoutubeVideoPlaybackStatusUnstarted:
            self.playing = NO;
            [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
            break;
            
        case YoutubeVideoPlaybackStatusEnded:
            self.playing = NO;
            [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
            break;
            
        case YoutubeVideoPlaybackStatusPlaying:
            self.playing = YES;
            [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
            break;
            
        case YoutubeVideoPlaybackStatusPaused:
            self.playing = NO;
            [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
            break;
            
        case YoutubeVideoPlaybackStatusBuffering:
            if (self.playing) {
                [self.playButton setTitle:@"Pause" forState:UIControlStateNormal];
            } else {
                [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
            }
            break;
            
        case YoutubeVideoPlaybackStatusVideoCued:
            self.playing = NO;
            [self.playButton setTitle:@"Play" forState:UIControlStateNormal];
            break;
    }
}

- (void)seek:(BOOL)seeksBackwards {
    NSString *time = [self.webView stringByEvaluatingJavaScriptFromString:@"ytplayer.getCurrentTime()"];
    NSLog(@"Playback before: %@", time);
    CGFloat timeToSeek = (self.playing) ? kSecondsToSeekWhilePlaying : kSecondsToSeekWhilePaused;
    
    if (seeksBackwards) {
        timeToSeek *= -1;
    }
    
    CGFloat playback = [time floatValue] + timeToSeek;
    NSString *query = [NSString stringWithFormat:@"ytplayer.seekTo(%f, true)", playback];
    NSLog(@"Query: %@", query);
    [self.webView stringByEvaluatingJavaScriptFromString:query];
}

@end
