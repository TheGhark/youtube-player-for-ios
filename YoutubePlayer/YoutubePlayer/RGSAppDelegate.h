//
//  RGSAppDelegate.h
//  YoutubePlayer
//
//  Created by Camilo Rodriguez Gaviria on 10/02/14.
//  Copyright (c) 2014 Red Gears Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RGSAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
